/*jshint esversion: 6*/
class World{
    constructor(worldSize, startX, startY) {
        this.worldSize = 100;
        this.world = new Array(this.worldSize);
        for (var i = 0; i < worldSize; i++) {
            this.world[i] = new Array(this.worldSize);
            for (var j = 0; j < worldSize; j++) {
                this.world[i][j] = new GrassArea();
            }
        }
        this.currX = startX;
        this.currY = startY;
    };
    moveArea(direction){
        if(direction==="north"){
            this.currY = this.currY + 1;
        }else if(direction==="east"){
            this.currX = this.currX + 1;
        }else if(direction==="south"){
            this.currY = this.currY -1;
        }else if(direction==="west"){
            this.currX = this.currX -1;
        }
    };
    currentArea(){
        return this.world[this.currX][this.currY];
    }

};
class Terrain{
    constructor(color){
        this.color = color;
    };
};

class GrassArea extends Terrain{
    constructor(){
        super("#007D33");
        this.type = "grass"
    };

    generateArea(width, height){
        var entities = new Array(width);
        for (var i = 0; i < entities.length; i++) {
            entities[i] = new Array(height);
            for (var j = 0; j < height; j++) {
                entities[i][j] = new Array();
                entities[i][j].push(new Grass(i, j));
                // console.log();
                if(Math.random()<0.1){
                    entities[i][j].push(new Tree(i, j));
                }
            }
        }
       return entities; 
    };
};