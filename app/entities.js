/*jshint esversion: 6*/
class Entity {
    constructor(shape, color, bgColor, x, y, seethrough, passable) {
        this.shape = shape;
        this.color = color;
        this.bgColor = bgColor;
        this.x = x;
        this.y = y;
        this.seethrough = seethrough;
        this.passable = passable;
    };
};
class Player extends Entity {
    constructor(x, y) {
        super("@", "#99ff99", "", x, y, true, false);
        this.fov = new ROT.FOV.RecursiveShadowcasting(this.lightPasses);
        this.visibility(1);
        this.hp= 100;
    };
    act() {
        Game.engine.lock();
        window.addEventListener("keydown", this);
    };
    /* wait for user input; do stuff when user hits a key */
    validMove(x, y) {
        console.log(x, y);
        return Game.screen.entities[x][y][Game.screen.entities[x][y].length - 1].passable;
    };

    //Refactor
    moveArea(x,y){
        if(x===0){
            console.log("west");
            return "west";
        }
        return false;
    };

    handleEvent(e) {
        // prevent scrolling with arrow keyts
        if ([32, 37, 38, 39, 40].indexOf(e.keyCode) > -1) {
            e.preventDefault();
        }
        var code = e.keyCode;
        if (!(code in keyMap)) { return; }

        var dir = ROT.DIRS[8][keyMap[code]];
        var newX = this.x + dir[0];
        var newY = this.y + dir[1];

        if (this.validMove(newX, newY)) {
            Game.screen.entities[this.x][this.y].pop();
            Game.draw(this.x, this.y);
            this.x = newX;
            this.y = newY;
            Game.screen.entities[this.x][this.y].push(this);
        }
        
        //Refactor
        var area = this.moveArea(this.x, this.y);
        if(area !== false){
            Game.world.moveArea(area);
            Game.screen = new Screen(Math.round(Game.height/Game.fontSize), Math.round(Game.height/Game.fontSize), Game.world.currentArea())
            this.x=Game.screen.width-1;
            console.log(Game.screen.entities.length);
            console.log(Game.screen.entities[this.x][this.y]);
            console.log(this.x);
            Game.screen.entities[this.x][this.y].push(this);
            Game.drawAll();
            Game.draw(this.x, this.y);
        };
        
        Game.display.clear();
        this.visibility(keyMap[code]);
        window.removeEventListener("keydown", this);
        Game.engine.unlock()
    };
    lightPasses(x, y) {
        if (x < Game.screen.width && y < Game.screen.height && x > -1 && y > -1) {
            return Game.screen.entities[x][y][Game.screen.entities[x][y].length - 1].seethrough;
        } else {
            return false;
        }
    };

    visibility(dir) {
        this.fov.compute180(this.x, this.y, Game.screen.height, dir, function (x, y, r, visibility) {
            if (x < Game.screen.width && y < Game.screen.height && x > -1 && y > -1) {
                Game.draw(x, y);
            }
        });
    };

};

class Wall extends Entity {
    constructor(x, y, visible) {
        super(x, y, visible, false);
        this.draw();
    };
    draw() {
        Game.display.draw(this.x, this.y, "#", "#cccccc", "#a6a6a6");
    };
};

class Tree extends Entity {
    constructor(x, y) {
        super("T", "#996633", "", x, y, false, false);
    };
};

class Grass extends Entity {
    constructor(x, y) {
        // if(Math.random()>0.9){
        // super(",", "#007D33", "#003300", x, y, visible, true);
        // } else {
        super("~", "#007D33", "#003300", x, y, true, true);
        // }
    };
};
