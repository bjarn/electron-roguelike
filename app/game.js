/*jshint esversion: 6*/
var remote = require('electron').remote;
var Game = {
    display: null,
    engine: null,
    player: null,

    init: function () {
        //Generate world
        this.world = new World(100, 50, 50);

        //Calculate fontsize
        [this.width, this.height] = remote.getCurrentWindow().getSize();
        this.fontSize = Math.round(this.height)/50;
        
        //Initiate console
        var options = {
            width: Math.round(this.height/this.fontSize),
            height: Math.round(this.height/this.fontSize),
            fontSize: this.fontSize,
            forceSquareRatio: true,
            bg: "#000000",
            fontFamily: "DejaVu Sans Mono"
        };
        this.display = new ROT.Display(options);
        document.getElementById("mainContainer").appendChild(this.display.getContainer());


        //Current gamescreen
        this.screen = new Screen(options.width, options.height, this.world.currentArea());

        //Create and draw a player character
        this.player=this.createPlayer(Math.round(options.width/2), Math.round(options.height/2));

        this.scheduler = new ROT.Scheduler.Simple();
        this.scheduler.add(this.player, true);

        this.engine = new ROT.Engine(this.scheduler);
        this.engine.start();

    },

    createPlayer: function (x,y) {
        
        while (!Game.screen.entities[x][y][Game.screen.entities[x][y].length - 1].passable) {
            x = x + 1;
            y = y + 1;
        }
        player = new Player(x, y);
        Game.screen.entities[player.x][player.y].push(player);
        Game.draw(player.x, player.y);
        return player;
    },

    //Redraws the whole screen
    drawAll: function () {
        Game.display.clear();
        for (var i = 0; i < this.screen.entities.length; i++) {
            for (var j = 0; j < this.screen.entities[i].length; j++) {
                topIndex = this.screen.entities[i][j].length - 1;
                Game.display.draw(i, j, this.screen.entities[i][j][topIndex].shape, this.screen.entities[i][j][topIndex].color, this.screen.entities[i][j][0].bgColor);
            }
        }
    },
    //Redraw single object
    draw: function (x, y) {
        // console.log(x,y);
        topIndex = this.screen.entities[x][y].length - 1;
        Game.display.draw(x, y, this.screen.entities[x][y][topIndex].shape, this.screen.entities[x][y][topIndex].color, this.screen.entities[x][y][0].bgColor);
    }
};

class Screen {
    constructor(height, width, area) {
        this.width = width;
        this.height = height;
        this.entities = area.generateArea(this.width, this.height);
    };
};
